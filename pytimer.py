#!/usr/bin/env python

from dothat import lcd, touch
from dot3k.menu import Menu
from plugins.pomodoro.config import PomodoroConfig
from plugins.pomodoro.timer import PomodoroTimer

import sys
import os
import time

sys.path.append(os.getcwd())

if __name__ == "__main__":
    lcd.set_contrast(50)
    menu = Menu({
        'Pomodoro': {
            'Configure': PomodoroConfig(),
            'Start': PomodoroTimer()
        }
    },
        lcd,
        None,
        30)

    touch.bind_defaults(menu)
    while 1:
        menu.redraw()
        time.sleep(0.05)
