# README #

Get up and running with pytimer for Raspberry Pi and the Pimoroni Display-O-Tron

### What is pytimer for? ###

Configurable timers for the [Display-O-Tron HAT](https://shop.pimoroni.com/products/display-o-tron-hat)

### How do I get set up? ###

* Prerequisites

    Follow the Display-O-Tron setup guide [found here](https://github.com/pimoroni/displayotron/blob/master/README.md)
    Then clone this repository

* Running

    Run `pytimer.py`

### What plugins are available? ###

* Pomodoro

    The pomodoro method of time-tracking [described here](https://en.wikipedia.org/wiki/Pomodoro_Technique)

### How do I add my own plugins? ###

* Work in Progress