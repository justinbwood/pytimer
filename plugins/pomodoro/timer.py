from dot3k.menu import MenuOption
from dothat import backlight
from datetime import datetime, timedelta
import time


class PomodoroTimer(MenuOption):
    """
    Pomodoro Timer Plug-in
    """

    def __init__(self):
        self.running = False

        self.modes = ['worktime', 'resttime', 'workcycles', 'breaktime']
        self.mode_phrases = [' WORK CYCLE', ' REST CYCLE', '', '  BREAK  TIME!  ']
        self.default_options = [25, 5, 4, 15]
        self.mode_dict = dict(zip(self.modes, self.default_options))

        self.is_setup = False

        MenuOption.__init__(self)

    def begin(self):
        self.is_setup = False
        self.load_options()
        self.cycle_step = 0
        self.cycle_count = 1
        self.cycle_started = False
        self.running = True

    def setup(self, config):
        MenuOption.setup(self, config)
        self.load_options()

    def load_options(self):
        for key in self.mode_dict:
            self.mode_dict[key] = \
                int(self.get_option('Pomodoro', key, str(self.mode_dict[key])))

    def cleanup(self):
        self.running = False
        time.sleep(0.01)
        self.is_setup = False

    def redraw(self, menu):
        if not self.running:
            return False

        if self.idling:
            return True

        if not self.cycle_started:
            duration = self.mode_dict[self.modes[self.cycle_step]]
            self.end_time = datetime.now() + timedelta(minutes=duration)
            self.cycle_started = True
        elif self.end_time < datetime.now():
            if self.cycle_step == 0:
                self.cycle_step = 1
            elif self.cycle_step == 1:
                if self.cycle_count < self.mode_dict['workcycles']:
                    self.cycle_step = 0
                    self.cycle_count += 1
                else:
                    self.cycle_step = 3
            elif self.cycle_step == 3:
                self.cycle_step = 0
                self.cycle_count = 1
            self.cycle_started = False
        else:
            if self.cycle_step == 0:
                time_diff = self.end_time - datetime.now()
                time_remain = time_diff.seconds * 1000000 + time_diff.microseconds
                time_total = self.mode_dict['worktime'] * 60000000
                time_perc = float(time_remain) / float(time_total)
                hue_val = 0.333 - (0.333 * time_perc)
                backlight.hue(hue_val)
                backlight.set_graph(time_perc)
            elif self.cycle_step == 1:
                time_diff = self.end_time - datetime.now()
                time_remain = time_diff.seconds * 1000000 + time_diff.microseconds
                time_total = self.mode_dict['resttime'] * 60000000
                time_perc = float(time_remain) / float(time_total)
                hue_val = 0.333 * time_perc
                backlight.hue(hue_val)
                backlight.set_graph(time_perc)
            elif self.cycle_step == 3:
                time_diff = self.end_time - datetime.now()
                time_remain = time_diff.seconds * 1000000 + time_diff.microseconds
                time_total = self.mode_dict['breaktime'] * 60000000
                time_perc = float(time_remain) / float(time_total)
                time_offset = float(abs(time_remain % 6000000 - 3000000)) / 3000000.0
                blue_val = int(255 * time_offset)
                backlight.rgb(0, 0, blue_val)
                backlight.set_graph(time_perc)
            row_0 = self.mode_phrases[self.cycle_step]
            if not self.cycle_step == 3:
                row_0 += ' ' + str(self.cycle_count).zfill(3)
            row_1 = '   TIME  LEFT   '
            time_diff = self.end_time - datetime.now()
            minutes, seconds = divmod(time_diff.seconds, 60)
            row_2 = ' ' + str(minutes).zfill(2) + ' MIN  ' + \
                str(seconds).zfill(2) + ' SEC'
            menu.write_row(0, row_0)
            menu.write_row(1, row_1)
            menu.write_row(2, row_2)
