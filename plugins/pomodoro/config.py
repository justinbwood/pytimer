from dot3k.menu import MenuOption, MenuIcon


class PomodoroConfig(MenuOption):
    """
    Pomodoro Config Plug-in
    """

    def __init__(self):
        self.mode = 0
        self.modes = ['worktime', 'resttime', 'workcycles', 'breaktime']
        self.default_options = [25, 5, 4, 15]
        self.mode_dict = dict(zip(self.modes, self.default_options))

        self.icons_setup = False

        MenuOption.__init__(self)

    def begin(self):
        self.load_options()
        self.mode = 0

    def setup(self, config):
        MenuOption.setup(self, config)
        self.load_options()
        self.mode = 0

    def setup_icons(self, menu):
        menu.lcd.create_char(0, MenuIcon.arrow_left_right)
        menu.lcd.create_char(1, MenuIcon.arrow_up_down)
        menu.lcd.create_char(2, MenuIcon.arrow_left)
        self.icons_setup = True

    def update_options(self):
        for key in self.mode_dict:
            self.set_option('Pomodoro', key, str(self.mode_dict[key]))

    def load_options(self):
        for key in self.mode_dict:
            self.mode_dict[key] = \
                int(self.get_option('Pomodoro', key, str(self.mode_dict[key])))

    def down(self):
        self.mode += 1
        self.mode %= len(self.modes)
        return True

    def up(self):
        self.mode -= 1
        self.mode %= len(self.modes)
        return True

    def right(self):
        self.mode_dict[self.modes[self.mode]] += 1
        self.mode_dict[self.modes[self.mode]] %= 60
        self.update_options()
        return True

    def left(self):
        self.mode_dict[self.modes[self.mode]] -= 1
        self.mode_dict[self.modes[self.mode]] %= 60
        self.update_options()

        return True

    def cleanup(self):
        self.icons_setup = False

    def redraw(self, menu):
        if not self.icons_setup:
            self.setup_icons(menu)

        menu.write_row(0, chr(1) + 'Pomodoro')
        row_1 = 'Work ' + str(self.mode_dict['worktime']).zfill(2) + \
            ' Rest ' + str(self.mode_dict['resttime']).zfill(2)
        row_2 = 'Cycl ' + str(self.mode_dict['workcycles']).zfill(2) + \
            ' Brk  ' + str(self.mode_dict['breaktime']).zfill(2)

        row_1 = list(row_1)
        row_2 = list(row_2)
        icon_char = 0

        if self.mode == 0:
            row_1[4] = chr(icon_char)
        elif self.mode == 1:
            row_1[12] = chr(icon_char)
        elif self.mode == 2:
            row_2[4] = chr(icon_char)
        elif self.mode == 3:
            row_2[12] = chr(icon_char)

        menu.write_row(1, ''.join(row_1))
        menu.write_row(2, ''.join(row_2))
